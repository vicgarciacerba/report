package com.report.generateReport;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.query.JsonQueryExecuterFactory;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import org.json.JSONArray;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import net.sf.jasperreports.engine.data.JsonDataSource;
import sun.misc.BASE64Decoder;

@Path("/create")

public class ReportService {

    @POST
    @Path("/report")
    @Consumes(MediaType.APPLICATION_JSON)

    public String post(String id) {
        JSONObject respJson = new JSONObject();
        String pathToReports = System.getProperty("user.dir") + "/jasper/";
        Properties p = new Properties();
        try {
            //Carga el fichero de propiedades
            p.load(new FileReader(pathToReports + "properties.properties"));
            String fileOutput = pathToReports + p.getProperty("fileOutput");
            String fileFinalOutput = pathToReports + p.getProperty("fileFinalOutput");
            String fileDataJson = pathToReports + p.getProperty("fileDataJson");
            //Parametros definidos Jasper
            Map<String, Object> params = new HashMap<>();
            params.put(JsonQueryExecuterFactory.JSON_DATE_PATTERN, "yyyy-MM-dd");
            params.put(JsonQueryExecuterFactory.JSON_NUMBER_PATTERN, "#,##0.##");
            params.put(JsonQueryExecuterFactory.JSON_LOCALE, Locale.ENGLISH);
            params.put(JRParameter.REPORT_LOCALE, Locale.US);

            //Coge variables del JSON recibido 
            JSONObject jsonReport, jsonObjectReport, jsonObjectHeader = null;
            JSONArray jsonArrayAttachments = null;
            jsonReport = new JSONObject(id);
            String pathMain = pathToReports + jsonReport.getString("reportName") + ".jrxml";
            jsonObjectHeader = jsonReport.getJSONObject("header");

            String pathSubreportHeader1 = pathToReports + "headers/" + jsonObjectHeader.getString("subReportName") + ".jrxml";
            String pathSubreportHeader2 = pathToReports + "headers/" + jsonObjectHeader.getString("subReportName") + ".jasper";

            String pathSubreportTests1 = pathToReports + "tests/" + "subTests" + ".jrxml";
            String pathSubreportTests2 = pathToReports + "tests/" + "subTests" + ".jasper";
            params.put("tipoHeader", pathSubreportHeader2);
            params.put("tipoTests", pathSubreportTests2);
            params.put("dir", pathToReports);
            //Recoge los PDF incluidos 
            jsonArrayAttachments = jsonReport.getJSONObject("attachments").getJSONObject("data").getJSONArray("file");

            // Poner el contenido de lo que recibe en un fichero JSON para que el Jasper lo coja bien. Tambi�n le pone utf8 para que no peten los acentos en el jasper
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDataJson), "utf-8"));
            out.write(jsonReport.toString());
            out.close();
            //Compila Jasper
            JasperReport jasperReport = JasperCompileManager.compileReport(pathMain);
            ByteArrayInputStream jsonDataStream = new ByteArrayInputStream(jsonReport.toString().getBytes("UTF-8"));
            JsonDataSource ds = new JsonDataSource(jsonDataStream);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);
            JasperExportManager.exportReportToPdfFile(jasperPrint, fileOutput);
            JasperExportManager.exportReportToPdf(jasperPrint);

            //A?ade los ficheros externos y el jasper creado en un fichero 
            PDFMergerUtility ut = new PDFMergerUtility();
            ut.addSource(fileOutput);
            String pdfs = "";
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] decodedBytes = null;

            for (int i = 0; i < jsonArrayAttachments.length(); i++) {
                pdfs = jsonArrayAttachments.getString(i);
                decodedBytes = decoder.decodeBuffer(pdfs);
                ut.addSource(new ByteArrayInputStream(decodedBytes));
            }

            ut.setDestinationFileName(fileFinalOutput);
            ut.mergeDocuments();

            //Convierte el fichero creado en base64
            File originalFile = new File(fileFinalOutput);
            String encodedBase64 = null;

            FileInputStream fileInputStreamReader = new FileInputStream(originalFile);
            byte[] bytes = new byte[(int) originalFile.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = new String(Base64.encodeBase64(bytes));

            respJson.put("status", 1);
            respJson.put("msg", encodedBase64);
        } catch (JRException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
            try {
                respJson.put("status", "0");
                respJson.put("msg", ex);
            } catch (JSONException ex1) {
                Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex1);
            }

        } catch (JSONException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
            try {
                respJson.put("status", "0");
                respJson.put("msg", ex);
            } catch (JSONException ex1) {
                Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (IOException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
            try {
                respJson.put("status", "0");
                respJson.put("msg", ex);
            } catch (JSONException ex1) {
                Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return respJson.toString();
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }
}
