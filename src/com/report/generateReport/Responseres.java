package com.report.generateReport;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Responseres {
	private boolean status;
	private String message;
        private byte [] pdf;

	public boolean isStatus() {
		return status;
	}

    public byte[] getPdf() {
        return pdf;
    }

    public void setPdf(byte[] pdf) {
        this.pdf = pdf;
    }

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


        
}
